/*globals angular:false,console:false*/
angular.module('app.controllers', [])

    .controller('selectLocationCtrl', ['API', '$rootScope', '$scope', function (API, $rootScope, $scope) {
        "use strict";
        $scope.cities = [];
        $rootScope.showLoading('Loading cities');
        API.getCities().then(function (response) {
            console.log(response.data);
            if (response.data && !response.data.error) {
                $scope.cities = response.data.data;
                $rootScope.hideLoading();
            }
        }, function (error) {
            console.error(error);
        });
        $scope.selectCity = function (city) {
            $rootScope.city = city;
        };
    }])

    .controller('cartCtrl', ['$rootScope', '$scope', '$state', 'Auth', 'API', function ($rootScope, $scope, $state, Auth, API) {
        "use strict";
        var token = Auth.checkSession() ? Auth.getSession() : null;
        $scope.items = [];
        $scope.timeSlots = [];
        $scope.addresses = [];
        $scope.total = 0;
        API.getTimeSlots(1, 1).then(function (response) {
            console.log(response);
            if (response.data && !response.data.error) {
                $scope.timeSlots = response.data.data;
                $scope.selectedTimeSlot = $scope.timeSlots[0].id;
            }
            console.log($scope.timeSlots);
        }, function (error) {
            console.error(error);
        });
        API.getAddresses(token).then(function (response) {
            console.log(response);
            if (response.data && !response.data.error) {
                $scope.addresses = response.data.data;
                $scope.selectedAddress = $scope.addresses[0].id;
            }
            console.log($scope.addresses);
        }, function (error) {
            console.error(error);
        });
        $scope.$on('$stateChangeSuccess', function () {
            API.getCart(token).then(function (response) {
                console.log(response);
                if (response.data && !response.data.error) {
                    $scope.items = response.data.data;
                }
            }, function (error) {
                console.error(error);
            });
        });
        $scope.$watch(function watchItems(scope) {
            return scope.items;
        }, function handleItemsChanged() {
            $scope.updateTotal();
        });
        $scope.updateTotal = function () {
            var i, item, price, total = 0;
            for (i = 0; i < $scope.items.length; i++) {
                item = $scope.items[i];
                price = (+item.quantity) * (+item.stock_detail.marked_price);
                total += price;
            }
            $scope.total = total;
        };
        $scope.selectAddress = function (address) {
            $scope.selectedAddress = address;
        };
        $scope.selectTimeSlot = function (timeSlot) {
            $scope.timeSlot = timeSlot;
        };
        $scope.deleteItem = function (item, index) {
            $rootScope.showLoading('Removing item');
            API.deleteCartItem(item, token).then(function (response) {
                console.log(response);
                $scope.items.splice(index, 1);
                $scope.updateTotal();
                $rootScope.hideLoading();
                console.log($scope.items);
            }, function (error) {
                console.error(error);
            });
        };
        $scope.checkout = function (address, timeSlot) {
            console.log(address, timeSlot);
            API.checkout(1, address, timeSlot, token).then(function (response) {
                console.log(response);
                if (response.data && !response.data.error) {
                    console.log(response.data.message);
                    $rootScope.toast('Purchase completed!');
                    $state.go('selectLocation');
                }
            }, function (error) {
                console.error(error);
            });
        };
    }])

    .controller('profileCtrl', function ($scope) {
        "use strict";
    })

    .controller('categoriesCtrl', ['API', '$scope', '$rootScope', '$stateParams', function (API, $scope, $rootScope, $stateParams) {
        "use strict";
        var city = $stateParams.city;
        // $scope.city = city;
        $scope.categories = [];
        $rootScope.showLoading('Loading categories');
        API.getCategories(city, 1).then(function (response) {
            console.log(response.data);
            if (response.data && !response.data.error) {
                $scope.categories = response.data.data.map(function (item) {
                    return item.category;
                });
                $rootScope.hideLoading();
                console.log($scope.categories);
            }
        }, function (error) {
            console.error(error);
        });
        $scope.selectCategory = function (category) {
            $rootScope.category = category;
        };
    }])

    .controller('productsCtrl', ['API', '$rootScope', '$scope', '$stateParams', function (API, $rootScope, $scope, $stateParams) {
        "use strict";
        var category = $stateParams.category;
        // $scope.category = category;
        $scope.products = [];
        $rootScope.showLoading('Loading products');
        API.getProducts(category, 1).then(function (response) {
            console.log(response.data);
            if (response.data && !response.data.error) {
                $scope.products = response.data.data;
                $rootScope.hideLoading();
            }
        }, function (error) {
            console.log(error);
        });
        $scope.selectProduct = function (product) {
            $rootScope.product = product;
        };
    }])

    .controller('loginCtrl', ['$rootScope', '$scope', '$state', 'API', 'Auth', '$location', function ($rootScope, $scope, $state, API, Auth, $location) {
        "use strict";
        //$scope.email = null;
        //$scope.password = null;
        if (Auth.checkSession()) {
            API.refreshToken(Auth.getSession()).then(function (response) {
                console.log(response);
                if (response.data && response.data.token) {
                    Auth.createSession(response.data.token);
                }
            }, function (error) {
                console.error(error);
                Auth.deleteSession();
                $location.path('/login');
            });
            $rootScope.authenticated = true;
            $state.go('selectLocation');
        }

        $scope.login = function (email, password) {
            //console.log(email, password);
            /*if (Auth.checkSession()) {
                $state.go('selectLocation');
            }*/
            $rootScope.showLoading('Logging in');
            API.login(email, password).then(function (response) {
                console.log(response);
                if (response.data && !response.data.error) {
                    Auth.createSession(response.data.token);
                    $rootScope.hideLoading();
                    $state.go('selectLocation');
                } else {
                    console.error(response.data.message);
                    $rootScope.hideLoading();
                    $rootScope.toast(response.data.message);
                }
            }, function (error) {
                console.error(error);
                $rootScope.hideLoading();
                $rootScope.toast("Wrong Username or Password!");
            });
        };
    }])

    .controller('signupCtrl', ['$rootScope', '$scope', '$state', 'API', function ($rootScope, $scope, $state, API) {
        "use strict";

        $scope.signup = function (name, email, phone, password, repeat_password, address_1, address_2, pin) {
            var address = address_1 + ', ' + address_2 + ', Kolkata - ' + pin;
            $rootScope.showLoading('Signing up');
            //console.log(name);
            //console.log(email);
            //console.log(phone);
            //console.log(password);
            //console.log(repeat_password);
            //console.log(address);
            API.signup(name, email, password, repeat_password, address, phone).then(function (response) {
                console.log(response);
                $rootScope.hideLoading();
                if (response.data && !response.data.error) {
                    $rootScope.toast(response.data.message);
                    $state.go('login');
                } else {
                    $rootScope.toast(response.data.message);
                }
            }, function (error) {
                console.error(error);
                $rootScope.hideLoading();
                $rootScope.toast("Unable to create user!");
            });
        };
    }])

    .controller('adddeliveryaddressCtrl', ['$rootScope', '$scope', '$state', 'API', function($rootScope, $scope, $state, API) {
        "use strict";
        $scope.adddeliveryaddress = function (name, phone, address, pin) {
            API.getAreas(pin).then(function (response) {
                console.log(response);
                API.adddeliveryaddress(name, phone, address, response.data).then(function (response) {
                    console.log(response);
                    if (response.data && !response.data.error) {
                        $rootScope.toast(response.data.message);
                        $state.go('login');
                    } else {
                        $rootScope.toast(response.data.message);
                    }
                }, function (error) {
                    console.error(error);
                    $rootScope.toast("Unable to add delivery address!");
                });


            }, function (error) {
                console.error(error);
                $rootScope.toast("Unable to get areas!");
            });
        };
    }])

    .controller('confirmationCtrl', ['$scope', function ($scope) {
        "use strict";

    }])

    .controller('productDetailCtrl', ['$scope', '$rootScope', 'API', 'Auth', function ($scope, $rootScope, API, Auth) {
        "use strict";
        var token = Auth.checkSession() ? Auth.getSession() : null;
        // $scope.product = product;
        $scope.selectedVariant = 0;
        $scope.quantity = 0;
        /*$scope.selectVariant = function (variant) {
            console.log(variant);
            $scope.selectedVariant = variant;
        };*/
        $scope.addToCart = function (quantity, selectedVariant) {
            $rootScope.showLoading('Adding to cart');
            console.log(selectedVariant, Number(quantity));
            API.addToCart(selectedVariant, Number(quantity), 1, token).then(function (response) {
                console.log(response);
                $rootScope.hideLoading();
            }, function (error) {
                console.error(error);
            });
        };
    }]);
