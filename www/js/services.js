/*globals angular:false*/
angular.module('app.services', [])

    .service('API', ['WebAPI', function (API) {
        "use strict";
        return API;
    }])

    .service('DummyAPI', [function () {
        "use strict";
        var API = {};
        API.cities = [];
        return API;
    }])

    .service('WebAPI', ['$http', function ($http) {
        "use strict";
        var base = "http://localhost:8000/api",
            api = {};
        api.getBase = function () {
            return base;
        };
        api.getCities = function () {
            return $http.get(base + '/cities');
        };
        api.getArea = function (pincode) {
            return $http.get(base + '/areas?pincode=' + pincode);
        }
        api.getServices = function (city) {
            return $http.get(base + '/services?city=' + city);
        };
        api.getCategories = function (city, service) {
            return $http.get(base + '/nav-menu?service=' + service + '&city=' + city);
        };
        api.getProducts = function (category, service) {
            return $http.get(base + '/products?service=' + service + '&category=' + category);
        };
        api.login = function (email, password) {
            return $http.post(base + '/authenticate', {
                email: email,
                password: password
            });
        };
        api.signup = function (name, email, password, repeat_password, address, phone) {
            return $http.post(base + '/sign-up', {
                name: name,
                email: email,
                address: address,
                phone: phone,
                password: password,
                "repeat-password": repeat_password
            });
        };
        api.adddeliveryaddress = function (name, phone, address, area) {
            return $http.post(base + '/address/create', {
                name: name,
                address: address,
                phone: phone,
                area: area
            });
        };
        api.refreshToken = function (token) {
            return $http.get(base + '/token?token=' + token);
        };
        api.addToCart = function (stock, quantity, service, token) {
            return $http.post(base + '/cart?token=' + token, {
                stock: stock,
                quantity: quantity,
                service: service
            });
        };
        api.getCart = function (token) {
            return $http.get(base + '/cart?token=' + token);
        };
        api.deleteCartItem = function (item, token) {
            return $http.delete(base + '/cart/' + item + '?token=' + token);
        };
        api.getTimeSlots = function (city, service) {
            return $http.get(base + '/time-slots?service=' + service + '&city=' + city);
        };
        api.getAddresses = function (token) {
            return $http.get(base + '/address?token=' + token);
        };
        api.checkout = function (service, address, timeSlot, token) {
            return $http.post(base + '/order?token=' + token, {
                service: service,
                address: address,
                timeSlot: timeSlot
            });
        };
        return api;
    }])

    .service('Auth', ['$window', function ($window) {
        "use strict";
        var auth = {};
        auth.createSession = function (token) {
            $window.localStorage.token = token;
        };
        auth.getSession = function () {
            return $window.localStorage.token;
        };
        auth.deleteSession =  function () {
            delete $window.localStorage.token;
            return true;
        };
        auth.checkSession = function () {
            return !!$window.localStorage.token;
        };
        return auth;
    }]);

