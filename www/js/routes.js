/*globals angular:false*/
angular.module('app.routes', [])

    .config(function ($stateProvider, $urlRouterProvider) {
        "use strict";
        // Ionic uses AngularUI Router which uses the concept of states
        // Learn more here: https://github.com/angular-ui/ui-router
        // Set up the various states which the app can be in.
        // Each state's controller can be found in controllers.js
        $stateProvider

            .state('selectLocation', {
                url: '/location',
                templateUrl: 'templates/selectLocation.html',
                controller: 'selectLocationCtrl'
            })

            .state('zopKart.cart', {
                url: '/cart',
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/cart.html',
                        controller: 'cartCtrl'
                    }
                }
            })

            .state('zopKart.profile', {
                url: '/profile',
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/profile.html',
                        controller: 'profileCtrl'
                    }
                }
            })

            .state('zopKart', {
                url: '/menu',
                templateUrl: 'templates/zopKart.html',
                abstract: true
            })

            .state('zopKart.categories', {
                url: '/:city/categories',
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/categories.html',
                        controller: 'categoriesCtrl'
                    }
                }
            })

            .state('zopKart.products', {
                url: '/:category/products',
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/products.html',
                        controller: 'productsCtrl'
                    }
                }
            })

            .state('zopKart.confirmation', {
                url: '/confirmation',
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/confirmation.html',
                        controller: 'confirmationCtrl'
                    }
                }
            })

            .state('login', {
                url: '/login',
                templateUrl: 'templates/login.html',
                controller: 'loginCtrl'
            })

            .state('signup', {
                url: '/signup',
                templateUrl: 'templates/signup.html',
                controller: 'signupCtrl'
            })

            .state('zopKart.productDetail', {
                url: '/product/:product',
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/productDetail.html',
                        controller: 'productDetailCtrl'
                    }
                }
            });

        $urlRouterProvider.otherwise('/login');

    });
